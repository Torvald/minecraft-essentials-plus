package fr.torvald.minecraftEssentialsPlus;

import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;


/**
 * Hello world!
 *
 */
public class MinecraftEssentialsPlus extends JavaPlugin {
    @Override
    public void onEnable() {
        getServer().getConsoleSender().sendMessage(ChatColor.RED + "[MinecraftEssentialsPlus] " + ChatColor.WHITE + "Plugin started!");
    }

    @Override
    public void onDisable() {
        getServer().getConsoleSender().sendMessage(ChatColor.RED + "[MinecraftEssentialsPlus] " + ChatColor.WHITE + "Plugin stopped!");
    }
}
